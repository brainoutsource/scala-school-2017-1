package wtf.scala.lectures.e13

import doobie.imports._
import scalaz._, Scalaz._
import scalaz.concurrent.Task

object DoobieExample extends App {
  val xa = DriverManagerTransactor[Task](
    driver = "org.h2.Driver",
    url = "jdbc:h2:mem:example;DB_CLOSE_DELAY=-1",
    user = "",
    pass = ""
  )

  // table creation
  sql"""
      create table judges (
        id serial not null primary key,
        name varchar(64) not null,
        specialization varchar(64)
      );
      create table documents (
        owner_id varchar(64) not null,
        content varchar(64) not null
      );
  """.update.run.transact(xa).run

  // insert initial data
  Seq(
    ("Alice", Some("lawyer")),
    ("Bob", None),
    ("Chris", Some("property"))) foreach { case (name, property) =>
    sql"insert into judges (name, specialization) values ($name, $property)".update.run.transact(xa).run
  }

  Seq("aaa", "bbb", "ccc").zipWithIndex foreach { case (content, i) =>
    sql"insert into documents (owner_id, content) values (${i + 1}, $content)".update.run.transact(xa).run
  }

  val judges =
    sql"select * from judges"
      .query[Judge]
      .list
      .transact(xa)
      .run

  println(judges)
}
